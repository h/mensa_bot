# ATTENTION: This is really bad code. Do not look at this.

import access_tokens
import json
import requests
import telebot
from telebot import types
import datetime
import traceback
import time
import sys
import threading

bot = telebot.TeleBot(access_tokens.TELEGRAM_TOKEN)

def refresh_access_token():
    # response = requests.post("https://service.campus.rwth-aachen.de/oauth2/oauth2.svc/token",
    response = requests.post("https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/token",
            data={
                "client_id": access_tokens.CLIENT_ID,
                "refresh_token": access_tokens.REFRESH_TOKEN,
                "grant_type": "refresh_token"
            }
    )
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data["access_token"]

def is_token_still_valid():
    response = requests.post("https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/tokeninfo",
            data={
                "client_id": access_tokens.CLIENT_ID,
                "access_token": access_token
            }
    )
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data["state"] == "valid"


def ask_with_token(path, post_data):
    if not is_token_still_valid():
        global access_token
        access_token = refresh_access_token()
    post_data["token"] = access_token
    response = requests.get("https://moped.ecampus.rwth-aachen.de/proxy/api/v2/" + path, params=post_data)
    if response.status_code != 200:
        print(response.reason)
        return None
    data = json.loads(response.text)
    return data
    
access_token = refresh_access_token()

# Type emojis

emojis = {
    "pig": "🐷",
    "poultry": "🐔",
    "beef": "🐮",
    "fish": "🐟",
    "vegetarian": "🌱",
    "vegan": "🥑",
}

# verbosity is in {0, 1}
def parse_menu(response, verbosity, date="", weekday="", category="", contents=""):
    data = response["Data"]
    formatted_string = ""
    formatted_string += "*Plan für " + data["canteenName"] + "* "
    if category != "":
        formatted_string += "(Kategorie enthält " + category + ") "
    if contents != "":
        formatted_string += "\nNur Gerichte mit Label " + emojis[contents]
    formatted_string += "\n\n"
    for day in data["dayMenues"]:
        # Only include days that match weekday and date
        include_this = True
        for key, value in {"weekDay": weekday, "displayDate": date} \
            .items():
            if value != None and not value.lower() in day[key].lower():
                include_this = False
                break
        if not include_this:
            continue

        formatted_string += "_" + day["weekDay"] + ", " + day["displayDate"] + "_\n\n"

        # Menues sometimes have duplicates weirdly, we remove them here
        menues = [i for n, i in enumerate(day["menues"]) if i not in day["menues"][n + 1:]]
        menues.extend(day["supplements"])

        vegan_menues_by_category = {}
        vegetarian_menues_by_category = {}
        other_menues_by_category = {}

        for m in menues:
            if not should_include_menu(category, contents, m):
                continue

            menues_by_category = other_menues_by_category

            if "vegan" in m["mealTypes"]:
                menues_by_category = vegan_menues_by_category
            elif "vegetarian" in m["mealTypes"]:
                menues_by_category = vegetarian_menues_by_category

            if m["category"] not in menues_by_category:
                if "beilage" in m["category"]:
                    # For sorting
                    m["category"] = "_" + m["category"] + "_"

                menues_by_category[m["category"]] = "_" + m["category"] + "_ "

                if m["price"] != None:
                    menues_by_category[m["category"]] += " " + m["price"] + "\n"

            menues_by_category[m["category"]] += format_menu(verbosity, m)

        vegan_string = ""
        if len(vegan_menues_by_category) > 0:
            values = sorted(vegan_menues_by_category.values())
            vegan_string += f"{emojis['vegan']}:\n\n" + "\n".join(values) + "\n"

        vegetarian_string = ""
        if len(vegetarian_menues_by_category) > 0:
            values = sorted(vegetarian_menues_by_category.values())
            vegetarian_string += f"{emojis['vegetarian']}:\n\n" + "\n".join(values) + "\n"

        others_string = ""
        if len(other_menues_by_category) > 0:
            values = sorted(other_menues_by_category.values())
            others_string += f"Anderes:\n\n" + "\n".join(values) + "\n"

        formatted_string += ("—" * 10 + "\n").join(filter(lambda s: len(s) > 0, [vegan_string, vegetarian_string, others_string]))

    return formatted_string

def should_include_menu(category_query: str, meal_type_query: str, menu) -> bool:
    if menu["category"] is None:
        return False

    print(menu["category"])

    if (
        category_query != "" and
        category_query.lower() not in menu["category"].lower() and
        "beilage" not in menu["category"].lower()
    ):
        return False

    meal_type_query = meal_type_query.lower()

    if (
        menu["mealTypes"] is not None and
        meal_type_query != "" and
        all([meal_type_query != meal_type.lower() for meal_type in menu["mealTypes"]]) and
        "beilage" not in menu["category"].lower()
    ):
        return False

    if not menu["menu"]:
        return False

    return True

def format_menu(verbosity: int, menu) -> str:
    formatted_menu = ""

    menu_description = menu["menu"]

    if verbosity == 0:
        while "(" in menu_description and ")" in menu_description:
            begin = menu_description.index("(")
            end = menu_description[begin + 1:].index(")") + begin + 1
            menu_description = menu_description[:begin] + menu_description[end + 2:]

    menu_name = menu_description.split("|")[0]
    description_rest = menu_description.split("|")[1:]

    formatted_menu += "|".join(["*" + menu_name.strip() + "* "] + description_rest)

    formatted_menu = formatted_menu.strip() + " "

    if "mealTypes" in menu and len(menu["mealTypes"]) > 0:
        for meal_type in menu["mealTypes"]:
            if meal_type in {"vegan", "vegetarian"}:
                # These get their emoji displayed in the section header
                continue
            elif meal_type in emojis:
                formatted_menu += emojis[meal_type]
            else:
                formatted_menu += " " + meal_type + " "

    if verbosity >= 1:
        formatted_menu += "\n_Nährwert:_\n"
        for info in menu["nutritionInfo"]:
            formatted_menu += info + ": " + str(menu["nutritionInfo"][info]) + "\n"

    formatted_menu += "\n"

    return formatted_menu

# Mensa names
mensae = ask_with_token("Canteens/GetCanteens", {})
mensa_names = [it["canteenName"] for it in mensae["Data"]]
mensa_codes = {}
for mensa in mensae["Data"]:
    mensa_codes[mensa["canteenName"]] = mensa["canteenId"]

# Maps chat_ids to last executed command (heute, morgen, alles)
last_commands = {}
# Time frames (days) for commands
find_today = lambda: datetime.datetime.now().strftime("%d.%m.%Y")
find_tomorrow = lambda: (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d.%m.%Y")

time_frames = {
        "/heute": find_today,
        "/morgen": find_tomorrow,
        "/alles": lambda: ""
}

command_aliases = {
    "/vegan": "/heute academica vegan",
    "/vegetarian": "/heute academica vegetarian"
}

@bot.message_handler(commands=["vegan", "vegetarian"])
def aliases(message):
    message.text = command_aliases[message.text.split(" ")[0]]
    choose_mensa(message)

@bot.message_handler(commands=['license'])
def license(message):
    bot.reply_to(message, "This bot is FOSS! https://git.rwth-aachen.de/h/mensa_bot")

@bot.message_handler(commands=['start', 'help'])
def help(message):
    help_string = "*Hallo!* Ich bin der Mensabot.\n\n" + \
        "Kommandos: /heute, /morgen, /alles, /tag [Montag - Freitag]\n" + \
        "Mensa: Direkt spezifizieren mit Abkürzung, zB aca für Academica\n" + \
        "Kategorie: Direkt spezifizieren mit Abkürzung, zB klass für Klassiker\n" + \
        "Inhalt: `(" + "|".join(emojis.keys()) + ")` \n" + \
        "Optionen: -n für Ernährungswerte\n\n" \
        "Beispiele:\n" \
        "/heute -n academica\n" \
        "/morgen ahorn klassiker\n" \
        "/alles vita vegan\n" \
        "/tag mittwoch bistro\n" \
        "/morgen"
    bot.reply_to(message, help_string, parse_mode="markdown")

# Get a list of mensae
@bot.message_handler(commands=['heute', 'morgen', 'alles', 'tag'])
def choose_mensa(message):
    last_commands[message.chat.id] = message.text
    try:
        execute(message, None, message.text)
    except Exception as e:
        print(e)

# Get info from the selection
@bot.message_handler(regexp="(" + "|".join(mensa_names) + ")")
def simple_info(message):   
    last_command = "/heute"
    if message.chat.id in last_commands:
        last_command = last_commands[message.chat.id]
    try:
        execute(message, message.text, last_command)
    except Exception as e:
        print(e)
    
def execute(message, mensa_name, last_command):
    args = last_command.split(" ")
    additional_args = args[1:]

    time_frame = ""
    weekday = ""
    if args[0].split("@")[0] in time_frames:
        time_frame = time_frames[args[0].split("@")[0]]()
    elif args[0] == "/tag":
        if len(additional_args) == 0:
            return
        weekday = additional_args[0]
        additional_args.remove(weekday)

    verbosity = 0
    if "-n" in additional_args:
        additional_args.remove("-n")
        verbosity = 1

    found_mensa = mensa_name
    
    if mensa_name == None:
        for mensa_n in additional_args:
            for name in mensa_names:
                if mensa_n.lower() in name.lower():
                    mensa_name = name
                    additional_args.remove(mensa_n)
                    break
    
    # Look for contents (beef, vegan etc.) in arguments
    contents = ""
    if len(additional_args) > 0:
        for arg in additional_args:
            for cat in emojis.keys():
                if arg.lower() in cat.lower() or arg in emojis[cat]:
                    contents = cat
                    additional_args.remove(arg)
                    break

    possible_category = ""
    # Maybe there is a category in there (we know that it's not a day)
    if len(additional_args) > 0:
        possible_category = additional_args[0]

    # If we don't have a mensa name, supply one first
    if mensa_name == None:
        # Get current list of all cantines
        # Make a nice reply keyboard
        markup = types.ReplyKeyboardMarkup()
        for n in mensa_names:
            markup.add(types.KeyboardButton(n))
        bot.reply_to(message, "Bitte Mensa auswählen (oder nächstes mal in deine Nachricht" +
                " schreiben :))", reply_markup=markup)
        return

    
    canteen_id = mensa_codes[mensa_name]
    response = ask_with_token("Canteens/GetWeekMenu", {"canteenId": canteen_id})

    rich_text = parse_menu(response, verbosity, weekday=weekday, date=time_frame,
            category=possible_category, contents=contents)

    markup = types.ReplyKeyboardRemove(selective=False)

    lines = rich_text.split("\n")
    messages = []
    m = ""
    for l in lines:
        if len(m + l) < 4096:
            m += l + "\n"
        else:
            messages.append(m)
            m = ""
    messages.append(m)

    if len(messages) > 3:
        bot.reply_to(message, "Das ist leider zu lang. Sei genauer.",
                reply_markup=markup, parse_mode="markdown")
        return

    for substring in messages:
        bot.reply_to(message, substring, reply_markup=markup, parse_mode="markdown")

# https://github.com/eternnoir/pyTelegramBotAPI/issues/206
def telegram_polling():
    while True:
        try:
            print("Starting to poll..")
            bot.polling(none_stop=True)
        except:
            bot.stop_polling()
            traceback_error_string=traceback.format_exc()
            with open("error.log", "a") as myfile:
                myfile.write("\r\n\r\n" + time.strftime("%c")+"\r\n<<ERROR polling>>\r\n"+ traceback_error_string + "\r\n<<ERROR polling>>")
                print(traceback_error_string)
            time.sleep(10)

polling_thread = threading.Thread(target=telegram_polling)
polling_thread.daemon = True
polling_thread.start()

#Keep main program running while bot runs threaded
if __name__ == "__main__":
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
